<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class itemsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
            $today =date('Y-m-d');
        return [
            'username'=>'required',
            'starting_date'=>'required|date|date_format:Y-m-d|after:'.$today,
            'days_avalible'=>'required',
            'lecs_per_chapter'=>'required|integer:10|between:1,10',
        ];
    }
}
