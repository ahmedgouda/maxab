<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\item;

use App\Http\Requests\itemsRequest;
use function GuzzleHttp\json_encode;

use Illuminate\Validation\ValidationException;

class itemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jsonString = file_get_contents(base_path('items.json'));
        $data = json_decode($jsonString, true);
        return json_encode($data, JSON_PRETTY_PRINT);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('items');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(itemsRequest $request)
    {
        $username = $request->input('username');
        $starting_date = $request->input('starting_date');
        $days_available = ($request->input('days_avalible'));
        sort($days_available);
        $days_available = serialize($days_available);
        $lecs_per_chapter = $request->input('lecs_per_chapter');

        $item = new item();
        $item->username = $username;
        $item->starting_date = $starting_date;
        $item->days_available = $days_available;
        $item->lecs_per_chapter = $lecs_per_chapter;
        // $item->save();
        return $lecturesTable = $this->generate_lecture_table($item);
    }

    public function generate_lecture_table(item $item)
    {
        $chapters = 30;
        $starting_date = $item->starting_date ;
        $days_available = unserialize($item->days_available);
        $lecs_per_chapter =  $item->lecs_per_chapter ;
        $total_lecs = $chapters * $lecs_per_chapter;
        $lectuers_date_time = [];

        // first day to take a lec
        $lecture_date = $starting_date;
        for ($i=1; $i <= $total_lecs; $i++) {
            $current_chapter = ($i % $lecs_per_chapter) ? (int)($i / $lecs_per_chapter) + 1 : ($i / $lecs_per_chapter);
            if (!in_array($this->date_to_day_of_week($lecture_date), $days_available)) {

              $lecture_date =  $this->generate_nearby_available_date($lecture_date, $days_available);
            }
                $lecture = new \stdClass();
                $lecture->id = $i;
                $lecture->date = $lecture_date;
                $lecture->chapter = $current_chapter;
                $lecture->day_of_week = date("l", strtotime( $lecture_date));
                $lecture->time_from = "02:00 pm";
                $lecture->time_to = "04:00 pm";
                array_push($lectuers_date_time, $lecture);

                $lecture_date = $this->generate_next_available_date($lecture_date, $days_available);
        }
        return $lectuers_date_time;
    }

    public function  generate_nearby_available_date($lecture_date, $days_available){
        $day_of_week = $this->date_to_day_of_week($lecture_date);
        array_push($days_available, $day_of_week );
        sort($days_available);
        $index_of_day_not_available = array_search($day_of_week, $days_available);
        $neaby_day = $days_available[ ($index_of_day_not_available + 1) % count($days_available)];
        $diff_between_dates = $neaby_day - $day_of_week ;
        $diff_between_dates = $diff_between_dates > 0 ? $diff_between_dates : $diff_between_dates + 7;
        return date('Y-m-d',strtotime("+".$diff_between_dates." day", strtotime($lecture_date)));
    }
    public function generate_next_available_date($last_date, $days_available)
    {
        $last_day_available = $this->date_to_day_of_week($last_date);
        $index_of_last_dat_available = array_search($last_day_available, $days_available);
        $next_day_available = $days_available[($index_of_last_dat_available+1) % count($days_available)];
        $diff_between_dates = ($next_day_available - $last_day_available);
        $diff_between_dates = $diff_between_dates > 0 ? $diff_between_dates :$diff_between_dates + 7;
        return date('Y-m-d',strtotime("+".$diff_between_dates." day", strtotime($last_date)));
    }
    public function date_to_day_of_week($date)
    {
        return date("N", strtotime($date));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
