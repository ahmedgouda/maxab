import Vue from 'vue'
import App from './App.vue'
import Router from './router.js'
import VueResource from 'vue-resource'
import 'beautify-scrollbar/dist/index.css';
import 'v2-table/dist/index.css';
import V2Table from 'v2-table';
import { store } from './store/store.js';
Vue.use(V2Table)
Vue.use(VueResource);

new Vue({
  store: store,
  el: '#app',
  render: h => h(App),
  router: Router
})
