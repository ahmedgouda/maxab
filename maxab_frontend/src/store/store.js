import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
      lectures:[],
      username: ''
    },
     mutations: {
      change_table: (state,new_table) =>{
        state.lectures = new_table;
      },
      change_username: (state,new_username) =>{
        state.username = new_username;
      }
    },
    actions: {
      change_table: (context,new_table) => {
        context.commit('change_table',new_table);
      },
      change_username:  (context,new_username) => {
        context.commit('change_username',new_username);
      }
    }
});
