import Vue from 'vue'
import VueRouter from 'vue-router'

import AddItem from './components/AddItem.vue';
import Items from './components/items.vue';
Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    {
      path: "/",
      component: AddItem
    },
     {
      path: "/items",
      component: Items,
     }
  ]
});

export default router ;
